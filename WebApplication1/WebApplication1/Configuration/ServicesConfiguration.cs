﻿using System;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using WebApplication1.Health;
using WebApplication1.Infrastructure;
using WebApplication1.Infrastructure.Authorization;
using WebApplication1.Infrastructure.Authorization.Basic;
using WebApplication1.Infrastructure.Authorization.Handlers;
using WebApplication1.Infrastructure.Authorization.Requirements;
using WebApplication1.Models;
using WebApplication1.Models.Identity;

namespace WebApplication1.Configuration
{
    public static class ServicesConfiguration
    {
        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<RandomDigitProvider>();
            services.AddSingleton<DumbMiddleware>();

            ConfigureHealthChecks(services, configuration);
            ConfigureDatabase(services, configuration);
            ConfigureAuth(services, configuration);
        }


        private static void ConfigureHealthChecks(IServiceCollection services, IConfiguration configuration)
        {
            services.AddHealthChecks()
                .AddDbContextCheck<ApplicationContext>(name: HealthChecksNames.DbHealthCheck)
                .AddCheck<ThirdPartyHealthCheck>(HealthChecksNames.ThirdPartyHealthCheck);
        }

        private static void ConfigureDatabase(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
        }

        private static void ConfigureAuth(IServiceCollection services, IConfiguration configuration)
        {
            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationContext>();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 3;
                options.Password.RequiredUniqueChars = 1;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;
            });

            services.AddAuthentication()
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
                {

                })
                .AddBasic<AuthenticationService>(o =>
                {
                    o.Realm = "My App";
                });

            services.AddAuthorization(options =>
            {
                // below removed due to registering of my own policy provider, so you can drive age requirements dynamic

                // options.AddPolicy(Constants.Policies.MinimumAge21, policy => policy.Requirements.Add(new MinimumAgeRequirement(21)));
                options.AddPolicy(Constants.Policies.EmployeeOnly, policy => policy.RequireClaim(Constants.ClaimsTypes.IsEmployee));
            });

            services.AddSingleton<IAuthorizationPolicyProvider, MinimumAgePolicyProvider>();
            services.AddSingleton<IAuthorizationPolicyProvider, SexPolicyProvider>();
            services.AddSingleton<IAuthorizationHandler, MinimumAgeRequirementHandler>();
            services.AddSingleton<IAuthorizationHandler, SexRequirementHandler>();
            services.AddScoped<IUserClaimsPrincipalFactory<User>,
                AdditionalUserClaimsPrincipalFactory>();
            services
                .AddScoped<IUserStore<User>, MyUserStore>();
        }
    }
}
