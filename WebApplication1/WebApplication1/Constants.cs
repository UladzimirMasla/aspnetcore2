﻿namespace WebApplication1
{
    public static class Constants
    {
        public static class Policies
        {
            public const string MinimumAge21 = "MinimumAge21";
            public const string EmployeeOnly = "EmployeeOnly";
        }

        public static class ClaimsTypes
        {
            public const string BirthYear = "BirthYear";
            public const string IsEmployee = "IsEmployee";
            public const string Sex = "Sex";
        }
    }
}
