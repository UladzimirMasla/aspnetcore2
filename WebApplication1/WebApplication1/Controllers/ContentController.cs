﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Infrastructure.Authorization;
using WebApplication1.Infrastructure.Authorization.Requirements;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class ContentController : Controller
    {
        [SexAuthorize(Sex.Female)]
        public IActionResult Authorized()
        {
            return Content("Hello, authorized user!");
        }

        [SexAuthorize(Sex.Male)]
        public IActionResult MaleEndpoint()
        {
            return Content("You are male");
        }

        [MinimumAgeAuthorize(80)]
        public IActionResult Adult()
        {
            return View();
        }

        [Authorize(Policy = Constants.Policies.EmployeeOnly)]
        public IActionResult Employee()
        {
            return View();
        }
    }
}
