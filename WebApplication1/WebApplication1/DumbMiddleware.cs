﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebApplication1.Infrastructure;

namespace WebApplication1
{
    public class DumbMiddleware : IMiddleware
    {
        private RandomDigitProvider _randomDigitProvider;


        public DumbMiddleware(RandomDigitProvider randomDigitProvider)
        {
            _randomDigitProvider = randomDigitProvider;
        }


        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var value = _randomDigitProvider.GetDigit();

            await next.Invoke(context);
        }
    }
}
