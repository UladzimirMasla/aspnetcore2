﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using System;
using System.Threading.Tasks;
using WebApplication1.Infrastructure;

namespace WebApplication1
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlingMiddleware> _logger;
        private readonly RandomDigitProvider _randomDigitProvider;

        public ErrorHandlingMiddleware(RequestDelegate next, ILogger<ErrorHandlingMiddleware> logger, RandomDigitProvider randomDigitProvider)
        {
            _next = next;
            _logger = logger;
            _randomDigitProvider = randomDigitProvider;
        }


        public async Task InvokeAsync(HttpContext context, RandomDigitProvider randomDigitProvider)
        {
            try
            {
                var value = randomDigitProvider.GetDigit();
                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error catch in ErrorLoggingMiddleware");
                throw;
            }
        }
    }
}
