﻿namespace WebApplication1.Health
{
    public static class HealthChecksNames
    {
        public const string DbHealthCheck = "db_health";
        public const string ThirdPartyHealthCheck = "ThirdPartyHealthCheck";
    }
}
