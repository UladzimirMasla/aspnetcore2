﻿using Microsoft.Extensions.Diagnostics.HealthChecks;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace WebApplication1.Health
{
    public class ThirdPartyHealthCheck : IHealthCheck
    {
        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            var rand = new Random();
            var responceTime = rand.Next(100);

            if(responceTime > 70)
            {
                return Task.FromResult(HealthCheckResult.Degraded("The response from third party is too slow"));
            }

            return Task.FromResult(
                HealthCheckResult.Healthy("Up and running!"));
        }
    }
}
