﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using WebApplication1.Infrastructure.Authorization.Requirements;
using WebApplication1.Models.Identity;

namespace WebApplication1.Infrastructure.Authorization
{
    public class AdditionalUserClaimsPrincipalFactory : UserClaimsPrincipalFactory<User>
    {
		public AdditionalUserClaimsPrincipalFactory(
			UserManager<User> userManager,
			IOptions<IdentityOptions> optionsAccessor)
			: base(userManager, optionsAccessor)
		{ }


		public async override Task<ClaimsPrincipal> CreateAsync(User user)
		{
			var principal = await base.CreateAsync(user);
			var identity = (ClaimsIdentity)principal.Identity;

			var claims = new List<Claim>();
			claims.Add(new Claim(Constants.ClaimsTypes.BirthYear, user.Year.ToString()));

			if(user.IsEmployee)
            {
				claims.Add(new Claim(Constants.ClaimsTypes.IsEmployee, "true"));
			}

			claims.Add(new Claim(Constants.ClaimsTypes.Sex, Sex.Male.ToString()));

			identity.AddClaims(claims);

			return principal;
		}
	}
}
