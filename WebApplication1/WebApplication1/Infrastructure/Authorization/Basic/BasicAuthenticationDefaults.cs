﻿namespace WebApplication1.Infrastructure.Authorization.Basic
{
    internal class BasicAuthenticationDefaults
    {
        public const string AuthenticationScheme = "Basic";
    }
}