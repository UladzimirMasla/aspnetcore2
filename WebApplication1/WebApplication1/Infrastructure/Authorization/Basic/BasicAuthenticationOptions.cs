﻿using Microsoft.AspNetCore.Authentication;

namespace WebApplication1.Infrastructure.Authorization.Basic
{
    public class BasicAuthenticationOptions : AuthenticationSchemeOptions
    {
        public string Realm { get; set; }
    }
}