﻿using System.Threading.Tasks;

namespace WebApplication1.Infrastructure.Authorization.Basic
{
    public interface IBasicAuthenticationService
    {
        Task<bool> IsValidUserAsync(string user, string password);
    }
}