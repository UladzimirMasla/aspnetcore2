﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;

using WebApplication1.Infrastructure.Authorization.Requirements;

namespace WebApplication1.Infrastructure.Authorization.Handlers
{
    public class MinimumAgeRequirementHandler : AuthorizationHandler<MinimumAgeRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, MinimumAgeRequirement requirement)
        {
            if (!context.User.HasClaim(c => c.Type == Constants.ClaimsTypes.BirthYear))
            {
                return Task.CompletedTask;
            }

            var birthYear = context.User.FindFirst(c => c.Type == Constants.ClaimsTypes.BirthYear).Value;

            if(birthYear == null)
            {
                return Task.CompletedTask;
            }

            var isParsingSucceed = int.TryParse(birthYear, out var birthYearConverted);

            if(!isParsingSucceed)
            {
                return Task.CompletedTask;
            }

            if(birthYearConverted >= requirement.MinimumAge)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
