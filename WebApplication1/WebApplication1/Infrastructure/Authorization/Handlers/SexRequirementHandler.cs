﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Infrastructure.Authorization.Requirements;

namespace WebApplication1.Infrastructure.Authorization.Handlers
{
    public class SexRequirementHandler : IAuthorizationHandler
    {
        public Task HandleAsync(AuthorizationHandlerContext context)
        {
            var genderRequirement = context.PendingRequirements
                .FirstOrDefault(requirement => requirement is GenderRequirement)
                as GenderRequirement;

            if(genderRequirement != null)
            {
                var userSexClaim = context.User.FindFirst(uc => uc.Type == Constants.ClaimsTypes.Sex);

                if(userSexClaim == null)
                {
                    return Task.CompletedTask;
                }

                var userGender = (Sex) Enum.Parse(typeof(Sex), userSexClaim.Value);

                if(userGender == genderRequirement.Sex)
                {
                    context.Succeed(genderRequirement);
                }
            }

            return Task.CompletedTask;
        }
    }
}
