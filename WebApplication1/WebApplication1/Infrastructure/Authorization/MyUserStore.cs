﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

using WebApplication1.Models;
using WebApplication1.Models.Identity;

namespace WebApplication1.Infrastructure.Authorization
{
    public class MyUserStore : IUserStore<User>, IUserPasswordStore<User>, IUserRoleStore<User>
    {
        ApplicationContext _context;
        private readonly IdentityErrorDescriber _describer;

        protected DbSet<User> Users => this._context.Users;
        protected DbSet<IdentityRole> Roles => this._context.Roles;
        protected DbSet<IdentityUserRole<string>> UserRoles => this._context.UserRoles;


        public MyUserStore(
            ApplicationContext context,
            IdentityErrorDescriber describer
        )
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _describer = describer ?? throw new ArgumentNullException(nameof(describer));
        }


        public async Task<IdentityResult> CreateAsync(
            User user,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            try
            {
                await Users.AddAsync(user);

                await SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateConcurrencyException)
            {
                return IdentityResult.Failed(_describer.ConcurrencyFailure());
            }

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> DeleteAsync(
            User user,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            try
            {
                Users.Remove(user);

                await SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateConcurrencyException)
            {
                return IdentityResult.Failed(_describer.ConcurrencyFailure());
            }

            return IdentityResult.Success;
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        public async Task<User> FindByIdAsync(
            string userId,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            return await Users
                .FirstOrDefaultAsync(u => u.Id == userId, cancellationToken);
        }

        public async Task<User> FindByNameAsync(
            string normalizedUserName,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            return await Users
                .FirstOrDefaultAsync(u => u.NormalizedUserName == normalizedUserName, cancellationToken);
        }

        public Task<string> GetNormalizedUserNameAsync(
            User user,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(user.NormalizedUserName);
        }

        public Task<string> GetUserIdAsync(
            User user,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(user.Id.ToString());
        }

        public Task<string> GetUserNameAsync(
            User user,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(user.UserName);
        }

        public Task SetNormalizedUserNameAsync(
            User user,
            string normalizedName,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            user.NormalizedUserName = normalizedName;

            return Task.CompletedTask;
        }

        public Task SetUserNameAsync(
            User user,
            string userName,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            user.UserName = userName;

            return Task.CompletedTask;
        }

        public async Task<IdentityResult> UpdateAsync(
            User user,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            try
            {
                _context.Attach(user);

                await SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateConcurrencyException)
            {
                return IdentityResult.Failed(_describer.ConcurrencyFailure());
            }

            return IdentityResult.Success;
        }

        public Task SetPasswordHashAsync(
            User user,
            string passwordHash,
            CancellationToken cancellationToken
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            user.PasswordHash = passwordHash;

            return Task.CompletedTask;
        }

        public Task<string> GetPasswordHashAsync(
            User user,
            CancellationToken cancellationToken
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(user.PasswordHash!);
        }

        public Task<bool> HasPasswordAsync(
            User user,
            CancellationToken cancellationToken
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            return Task.FromResult(user.PasswordHash is not null);
        }

        public async Task AddToRoleAsync(
            User user,
            string roleName,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            var role = await Roles
                .FirstAsync(r => r.Name == roleName
                    || r.NormalizedName == roleName
                );

            var userRole = new IdentityUserRole<string>()
            {
                RoleId = role.Name,
                UserId = user.Id
            };


            UserRoles.Add(userRole);
        }

        public async Task RemoveFromRoleAsync(
            User user,
            string roleName,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            var role = await Roles
                .FirstAsync(r => r.Name == roleName
                    || r.NormalizedName == roleName
                );

            var userRole = await UserRoles.FirstAsync(ur => ur.RoleId == role.Id && ur.UserId == user.Id);

            UserRoles.Remove(userRole);
        }

        public async Task<IList<string>> GetRolesAsync(
            User user,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            var rolesIds = await UserRoles.Where(ur => ur.UserId == user.Id)
                .Select(ur => ur.RoleId)
                .ToListAsync();

            return await Roles
                .Where(r => rolesIds.Contains(r.Id))
                .Select(r => r.Name)
                .ToListAsync();
        }

        public async Task<bool> IsInRoleAsync(
            User user,
            string roleName,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            var roleId = Roles.First(r => r.Name == roleName).Id;

            return await UserRoles
                .AnyAsync(ur => ur.RoleId == roleId && ur.UserId == user.Id);
        }

        public async Task<IList<User>> GetUsersInRoleAsync(
            string roleName,
            CancellationToken cancellationToken = default
        )
        {
            cancellationToken.ThrowIfCancellationRequested();

            var roleId = Roles.First(r => r.Name == roleName).Id;

            var userIds = await UserRoles
                .Where(ur => ur.RoleId == roleId)
                .Select(ur => ur.UserId)
                .ToListAsync();

            var users = await Users
                .Where(u => userIds.Contains(u.Id))
                .ToListAsync();

            return users;
        }

        private async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
