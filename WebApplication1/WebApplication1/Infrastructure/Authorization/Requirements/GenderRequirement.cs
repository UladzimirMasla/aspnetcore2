﻿using Microsoft.AspNetCore.Authorization;

namespace WebApplication1.Infrastructure.Authorization.Requirements
{
    public class GenderRequirement : IAuthorizationRequirement
    {
        public Sex Sex;

        public GenderRequirement(Sex sex)
        {
            Sex = sex;
        }
    }
}
