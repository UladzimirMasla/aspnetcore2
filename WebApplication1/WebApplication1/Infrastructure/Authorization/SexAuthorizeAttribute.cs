﻿using Microsoft.AspNetCore.Authorization;
using System;
using WebApplication1.Infrastructure.Authorization.Requirements;

namespace WebApplication1.Infrastructure.Authorization
{
    public class SexAuthorizeAttribute : AuthorizeAttribute
    {
        const string POLICY_PREFIX = "Sex";

        public SexAuthorizeAttribute(Sex sex) => Sex = sex;

        public Sex Sex
        {
            get
            {
                if (Enum.TryParse(typeof(Sex), Policy.Substring(POLICY_PREFIX.Length), out var sex))
                {
                    return (Sex)sex;
                }

                return default(Sex);
            }
            set
            {
                Policy = $"{POLICY_PREFIX}{value}";
            }
        }
    }
}
