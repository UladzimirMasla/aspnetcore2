﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using WebApplication1.Infrastructure.Authorization.Requirements;

namespace WebApplication1.Infrastructure.Authorization
{
    public class SexPolicyProvider : IAuthorizationPolicyProvider
    {
        const string POLICY_PREFIX = "Sex";

        public DefaultAuthorizationPolicyProvider FallbackPolicyProvider { get; }


        public SexPolicyProvider(IOptions<AuthorizationOptions> options)
        {
            FallbackPolicyProvider = new DefaultAuthorizationPolicyProvider(options);
        }


        public Task<AuthorizationPolicy> GetDefaultPolicyAsync() => FallbackPolicyProvider.GetDefaultPolicyAsync();

        public Task<AuthorizationPolicy> GetFallbackPolicyAsync() => FallbackPolicyProvider.GetFallbackPolicyAsync();

        public Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {
            if (policyName.StartsWith(POLICY_PREFIX, StringComparison.OrdinalIgnoreCase) &&
                Enum.TryParse(typeof(Sex), policyName.Substring(POLICY_PREFIX.Length), out var sex))
            {
                var policy = new AuthorizationPolicyBuilder();
                policy.AddRequirements(new GenderRequirement((Sex)sex));

                return Task.FromResult(policy.Build());
            }

            return FallbackPolicyProvider.GetPolicyAsync(policyName);
        }
    }
}
