﻿using System;

namespace WebApplication1.Infrastructure
{
    public class RandomDigitProvider
    {
        private readonly int digit;


        public RandomDigitProvider()
        {
            var rand = new Random();

            digit = rand.Next(100);
        }


        public int GetDigit()
        {
            return digit;
        }

    }
}
