﻿using Microsoft.AspNetCore.Identity;

namespace WebApplication1.Models.Identity
{
    public class User : IdentityUser
    {
        public int Year { get; set; }

        public bool IsEmployee { get; set; }
    }
}
